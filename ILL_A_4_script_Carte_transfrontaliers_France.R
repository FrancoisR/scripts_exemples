rm(list = ls())

# Chargement des packages

library(mapsf)

# Chargement table

Frontaliers2018 <- st_read("ILL_A_4_data_Travailleurs_transfrontaliers_2018.gpkg")


###############
### CARTES ###
##############

# ACTIFS AYANT UN EMPLOI OCCUPE A L'ETRANGER

Carte <- function(x, breaks, val_max, annee) {
  
  mf_export(x = x, filename = paste0("ILL_A_4_carte_NBFRONT_",annee,".png"), width = 800, 
            theme = "agolalight", expandBB = c(0, 0, 0, 0))
  mf_shadow(x, col = "grey10", add = TRUE)
  mf_map(x) %>%
    mf_prop_choro(c("Nbre_Transfront_Interco", "Ratio_Transfront_Interco"), 
                  val_max = val_max, breaks = breaks, nbreaks = 4,
                  leg_title = c("Nombre de\nfrontaliers", "frontaliers pour\n1000 actifs occupés"),
                  leg_pos = c('topright', 'right'))
  mf_title(paste0("Les travailleurs frontaliers par intercommunalité en ", annee), cex = 1.1)
  mf_credits("F. Reynaud. Sources : RP & IGN-F")
  mf_scale(size = 10)
  mf_arrow('topleft')
  dev.off()
}

Carte2018 <- Carte(Frontaliers2018, breaks = "jenks", val_max = 29432, "2018")
